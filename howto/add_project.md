# How to add a project to chained-ci

## 1-Pre-requisite

A git project named new_project is already defined and the willingness is to
adapt it to be run from chained-ci.

## 2-Initialize CI environment in this existing project

### a)Environment variables

If the project used input variables, they shall be defined as environment
variables to be able to set it in chained-ci project.  

### b)Add the sub-module chained-ci-tools to the project

To be able to manage artifacts exchanged between projects in the chain or to
configure the jumphost access, the chained-ci-tools submodule shall be added to
the project.

```shell
git submodule add -b master https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-tools.git scripts/chained-ci-tools
```

### c)Define a generic job in `.gitlab-ci.yml`

This defined generic job allows to initialize the ssh configuration towards the
jumphost server and to access remote artifacts.  
The management of artifacts (-a option) or inventory (with -i) is optional.  

```yaml
##
# Generic Jobs
##
.chained_ci_tools: &chained_ci_tools
  before_script:
    - ./scripts/chained-ci-tools/chained-ci-init.sh -a -i inventory/inventory
  after_script:
    - ./scripts/chained-ci-tools/clean.sh
```

This job is then added for each job:

```yaml
job1:
  <<: *chained_ci_tools
  stage: stage1
  script:
    - echo "Job1 actions"
  <<: *runner_tags
  only:
    refs:
      - triggers
jobs2:
  <<: *chained_ci_tools
  stage: stage2
  script:
    - echo "job2 actions"
  <<: *runner_tags
  only:
    refs:
      - triggers
```

In this example, the stages can only be launched using an api triggering
(identified by the keyword "triggers"). But it is a project decision to add it
to prevent other triggering solutions.

The following GIT variable shall be added to fetch the last sub-modules
versions within the runners:

```yaml
##
# Variables
##

variables:
  GIT_SUBMODULE_STRATEGY: recursive

```

### d)CI Trigger configuration

#### CI Trigger creation

In the web browser, in the settings menu, CI/CD part, select the sub-menu "Pipeline triggers".  
Add the trigger with the description of your choice.  
A token shall be now created.

#### ANSIBLE_VAULT_PASSWORD declaration

The delivered CI trigger token is not secured and will have to be encrypted
using the variable "ANSIBLE_VAULT_PASSWORD" that shall be defined in the
variable sub-menu of CI/CD settings menu.  
The value for this password can be a simple string like "chainedci".

### e)Additional VARIABLES declaration

In addition to ANSIBLE_VAULT_PASSWORD, additional variable PRIVATE_TOKEN to  
allow the access to artifacts from other projects in gitlab.com or another  
gitlab shall be declared.  

This token value is obtained selecting "Settings" menu of your gitlab profile  
and generating access token.

## 3-Update of your chained-ci project

Before defining a new scenario or chain, the project needs to be declared in
the file `all.yml` under pod_inventory/group_vars.  

* Add a new stage in stages section for the new project
* Set the configuration of the project in git_projects section

```yaml
# The stage declaration is mandatory to trigger the project
stages:
  - ...
  - new_project  
  
# The runner is the docker or vm where the chain will be executed
runner:
  tags:
    - docker
  env_vars:
    foo: bar
  docker_proxy:
  image: registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible
  image_tag: 2.7-alpine


gitlab:
  pipeline:
    delay: 15
  base_url: https://gitlab.com
  api_url: https://gitlab.com/api/v4
  private_token: "{{ lookup('env','CI_private_token') }}"

  git_projects:
    # Each git project is declared in this section
    # - API definition to trigger the remote project using the trigger token
    # - url of the project
    # - list of parameters to set for this project
    new_project:
      stage: new_project
      api: https://gitlab.com/api/v4/projects/9999999
      url: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/new_project
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        99999931343338643036313136306365333164386136626530636539353364323362376665666563
        6162636539633565363232613036323931343561366238370a306464383935373935616134353238
        65643933643032353038626537623435346331623634383338306430326461646138626162356332
        3665613732306430650a643031633736636361653536633361326233353337623437373535396231
        66366237636530373065383664363065643064323566316163366464386661699999
      branch: "{{ lookup('env','project_branch')|default('master', true) }}"
      timeout: 300
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"  
```

The trigger token is generated using the following command.

```shell
ansible-vault encrypt_string --vault-password-file vaultpassword.yml \
              <trigger token from new_project>
```

The file vaultpassword.yml contains the string defined in the variable
ANSIBLE_VAULT_PASSWORD.
