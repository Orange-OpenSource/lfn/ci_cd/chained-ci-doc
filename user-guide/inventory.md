# Inventory file description and purpose

Within chained-ci context, the inventory list all the chains that are
configured and shall be compiled in the CI file `.gitlab-ci.yml`.

## Structure

```YAML
[generic_group_1]
Chain_name_1a
chain_name_1b
[generic_group_2]
chain_name_2a
chain_name_2b
```

The group name is important to select the ssh credentials to reach the
jumphost server.

This ssh credential file is stored under pod_inventory/group_vars and is named
\<group name\>.yml following the current example.

Example `generic_group_1.yml`  

```YAML
---
ansible_ssh_creds: generic_group_creds.yml
```

The file `generic_group_creds.yml` is stored in the config directory whose path
is defined in `all.yml` in the git_projects section (config project).
It contains the private  and private vault_ssh_id_rsa keys.

```YAML
---
vault_ssh_id_rsa: |
  -----BEGIN RSA PRIVATE KEY-----
  ...
  -----END RSA PRIVATE KEY-----
vault_ssh_id_rsa_pub: |
  ssh-rsa ...
```
