# Create your chain

You must clone the chained-ci project.

```Shell
   git clone https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci
   cd chained-ci
   git submodule init
   git submodule update
```

The chained-ci repository can be shown as follows:

```Ascii
.
├── pod_inventory
│   ├── group_vars
│   │   └── all.yml
│   ├── host_vars
│   │   ├── existing_chained.yml
│   │   ├── ...
│   │   └── vpod1.yml
│   └── inventory
└── roles
    ├── artifact_init
    │   ├── defaults
    │   └── tasks
    ├── get_artifacts
    │   ├── defaults
    │   └── tasks
    ├── gitlab-ci-generator
    │   ├── tasks
    │   └── templates
    ├── run-ci
    │   └── tasks
    └── trigger_myself
        └── tasks
```

The ansible roles are retrieved through the git submodules.

Note: It is recommended to create your branch to create your new chain.

To create you chained-ci chain

* Edit  the `pod_inventory/inventory` file to declare your chain
* Create a file in `pod_inventory/host_vars` to configure your chain
* Edit `pod_inventory/group_vars/all.yaml` to reference some variables if
  needed

## Declare your chain

You must first declare your chain name in the `pod_inventory/inventory` file.
Edit the file and add your chain name at the end of the file.
The chain name shall be self-explicit.

Even if the naming is free, it is recommended to follow the syntax:

`[target][_[infra]]_[[ci_option]]_[pod]]`

e.g.

* `pod2`: deployment of the pod2 infrastructure (GNU/Linux OS)
* `acumos_pod4`: deployment of acumos on POD4
* `functest_pod4`: run functest tests on POD4
* `k8s_pod1`: deployment of kubernetes on bare metal on pod1
* ...
* `onap_k8s_daily_pod4`: daily deployment of onap on k8s infra installed on pod4

## Define your chain

The second step consists in creating the ansible role for the new chain.

Create a file in `pod_inventory/host_vars`.

The file name shall correspond to the chain name you declared in the inventory
file e.g. acumos_pod4.yaml.

This file describes the chain. It references the different projects you want to
chain, indicating the expected artifacts to be re-used from one project to
another.

```YAML
---
jumphost:
  server: <target POD jumphost IP>
  user: <ansible user>
[option section]
scenario_steps:
  step 1:
  ...
  step 2:
  ...
  step N:
  ...
```

The chain will execute the various steps in sequence or in parallel depending on 
the [gitlab stage] (https://docs.gitlab.com/ee/ci/yaml/#stages) associated with
each step.

The option section is not mandatory.

### jumphost Definition

A jumphost is associated to a chained-ci chain even if the different steps can
be performed through projects hosted in different environments (public
gitlab.com and internal gitlab). The creator of the chain shall ensure that the
projects can be invoked from the referenced jumphost.

In case no jumphost is requested in the created chain, the jumphost section
shall be present but can remain empty.

```YAML
---
jumphost:
[option section]
scenario_steps:
  step 1:
  ...
  step 2:
  ...
  step N:
  ...
```

### Chained-ci options

The possible option fields are:

* `environment`: this parameter is used and propagated as an environment variable
  to all the projects involved in the chained-ci
* `longlife_artifact`: `true` (not present by default)
* `inpod`: `<other_chained_ci_reference>`
* `healthchecks_id`: `<uid>`

#### longlife_artifact

The `longlife_artifact` option is needed to store the artifacts over a long
period. As other chains may consume the artifacts, if the chained-ci is not run
frequently, the artifacts could be deleted after a while and become unavailable
for other chained-ci chains.

Typically if you install an infrastructure once and run several chains
requesting the artifacts of the infrastructure installation, it makes sense to
set this option.

#### inpod


Chained-ci can be seen as matriochka, in fact you may trigger chain once
a previous chain has been completed.

The `inpod` parameter allows to define another chain that will trigger this chain.

![inpod](../img/inpod.png)

Considering the example `Chained-CI #2`, inpod parameter of pileline B' will
contain the value `Pipeline A`. The chain is launched setting POD variable with
the value `Pipeline A`.

#### healthchecks_id

The `healthcheck_id` option is used to reference your chained-ci to
[healthcheck](https://github.com/healthchecks/healthchecks).

### Chained-ci steps

Each step can be defined as follow:

```YAML
[step name]:
  project: [gitlab project reference]
  get_artifacts:
    - name: [project name for the artifact]
      [static_src: true]
  [extra parameters]
  [infra]
```

#### step name

Some predefind steps are defined:

* `config`
* `trigger`

##### config

This step is mandatory.

##### trigger

This optional step is used to trigger chains that must be executed after this chain
(typically using the `inpod` parameter).

#### gitlab project reference (mandatory)

It shall correspond to a name of a gitlab project. The project is defined in
file `all.yml` under pod_inventory/group_vars.

#### get_artifacts (optional)

The `get_artifacts` is to retrieved data (env variables, configuration files)
from a previous gitlab pipeline from a chained-ci.

See [how to manage artifacts](../howto/manage_artifacts.md) to discover all
options linked to artifact management.

These artifacts can be retrieved:

* dynamically (by Default): gitlab API will be called to retrieved the last
  artifacts corresponding to the project referenced
* statically: if static_src parameter is set to True, you then must indicate
  the path to the artifact

By default Chained-ci will call gitlab APIs to retrieved the artifact.

##### limit_to

Please note that it is possible to retrieve only a subset of the files from
an artifact by using `limit_to`.

```YAML
get_artifacts:
      - name: infra_deploy
        limit_to:
          - vars/user_cloud.yml: vars/user_cloud.yml
```

##### in_pipeline=false

It is also possible to indicate that the artifact cannot be retrieved from the
pipeline by setting in_pipeline=false in the get_artifact section. You then
have to add the name of the pipeline to fetch and the step(s) in the pipeline.

In the example, we retrieve artifacts from `config` step of last
`onap_k8s_ic_pod4` pipeline.

```YAML
 get_artifacts:
      - name: config:onap_k8s_ic_pod4
        in_pipeline: false
        limit_to:
          - vars/pdf.yml: vars/pdf.yml
          - vars/idf.yml: vars/idf.yml
```

#### extra parameters (optional)

For each project in the chain, extra-parameters can be defined. These
parameters are the environement variables used as input for the project. It
shall be configured as follows:

```YAML
extra_parameters:
      key 1: value 1
      key 2: value 2
      ....
      key N: value N
```

## Gitlab-ci generator

The `.gitlab-ci.yml` of your chained-ci project may be a little be tricky to
generate. That is why a gitlab-ci generator has been introduced.

The gitlab-ci generator is provided to generate automatically the file
`.gitlab-ci.yml` including your created chain. This generator is based on
the ansible role gitlab-ci-generator.

```Shell
ansible-playbook -i ./pod_inventory/inventory ./gitlab-ci-generator.yml
```

In the `PLAY RECAP` part of the playbook execution, you shall see your
chained-id name.

Once you have generated `.gitlab-ci.yml`, the changes have to be pushed to the
gitlab server (via a merge request or not, depending on your push policy)
before using it.

## chained-ci tools

This is a set of tools to prepare the CI environment of each project included
in the chain. The provided tools are:

* Get artifacts from a projects
* Set the configuration (ssh key and ssh config)
* Generate Vault key file

The following example shows the initialization of the CI environment in the file
`.gitlab-ci.yml` of a project included in a chained-ci chain.

```YAML
.chained_ci_tools: &chained_ci_tools
  before_script:
    - ./chained-ci-tools/chained-ci-init.sh -a -i inventory/inventory
  after_script:
    - ./chained-ci-tools/clean.sh

##
# Stages
##

job1-dummy1:
  <<: *chained_ci_tools
  <<: *runner
  stage: stage1
  script:
    - echo "this is a first job in stage 1"
```

The option -a allows to read the remote artifact built in previous project of
the chain.  
The -i option is to set the inventory file for ssh config.

See [README](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-tools)
of the project chained-ci-tools for details.
