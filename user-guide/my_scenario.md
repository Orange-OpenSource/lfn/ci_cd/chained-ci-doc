# Scenario file description

A scenario file defines a chain and is stored under pod_inventory/host_vars.  

It is composed of two sections:

* jumphost: This section provides the configuration information to join the
  jumphost server
  * server: FQDN to reach the server
  * user: linux user knowing that this information can be encrypted 
* scenario_steps: Scenario description indicating the git projects to chain

Of course, the different chained_ci options can be set in this file
(e.g inpod, get_artifacts...)

Here is a typical example:

```yaml
---
jumphost:
  server: myserver.orange.fr
  user: admin
scenario_steps:
  project1:
    project: chained-ci-dummy1
  project2:
    project: chained-ci-dummy2
```
