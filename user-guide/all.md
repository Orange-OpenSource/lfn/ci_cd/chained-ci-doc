# Generic variable file for chained-ci

## Structure definition
There are 4 sections in the variable file:

* protected_pods
* stages:
  There is a stage to define per new project in addition to stages for code
  review.
* runner: Definition of the gitlab runners to select for chained-ci
* gitlab:
  * Configuration information about gitlab solution hosting the chained-ci project
  * Configuration information about the projects to chain

## Typical example

```YAML
---
protected_pods:
stages:
  - lint
  - config
  - chained-ci-dummy1 
  

runner:
  tags:
    - docker
  env_vars:
    foo: bar
  docker_proxy:
  image: registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible
  image_tag: 2.7-alpine


gitlab:
  pipeline:
    delay: 15
  base_url: https://gitlab.com
  api_url: https://gitlab.com/api/v4
  private_token: "{{ lookup('env','CI_private_token') }}"

  git_projects:
    config:
      stage: config
      url: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci
      branch: "{{ lookup('env','config_branch')|default('master', true) }}"
      path: repo_config
   
    chained-ci-dummy1:
      stage: chained-ci-dummy1
      api: https://gitlab.com/api/v4/projects/9282482
      url: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-dummy1
      trigger_token: !vault |
        $ANSIBLE_VAULT;1.1;AES256
        63313931343338643036313136306365333164386136626530636539353364323362376665666563
        6162636539633565363232613036323931343561366238370a306464383935373935616134353238
        65643933643032353038626537623435346331623634383338306430326461646138626162356332
        3665613732306430650a643031633736636361653536633361326233353337623437373535396231
        66366237636530373065383664363065643064323566316163366464386661636466
      branch: "{{ lookup('env','dummy1_branch')|default('master', true) }}"
      timeout: 300
      parameters:
        ansible_verbose: "{{ lookup('env','ansible_verbose') }}"  
 
```
