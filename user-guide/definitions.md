# Chained CI Definitions

* __Scenario__: A POD name describing a set of steps linked to a target.
* __Target__: A set of servers, baremetal or virtual.
* __Step__: An unitary part of a scenario. The step is linked to an external
  gitlab project, compatible with chained-ci.
* __Chained CI Pipeline__: A specific execution of a scenario. Each pipeline will run the steps defined in the scenario, triggering the actions compiled in file `.gitlab-ci.yml`
