# Chained-ci

Chained-ci is used by OSONS team to automate and chain the deployment of
different systems and sub-systems.

Chained-ci already allows the automatic installation of the following modules:

* Infrastructure Installation (operating system, Openstack, Kubernetes)
* Infrastructure Testing (Openstack, Kubernetes)
* Cloud ressource creation on Openstack
* ONAP installation
* ONAP testing

The automatic installation is performed by Ansible roles available in gitlab
projects (either on gitlab.com or ib gitlab.forge.orange-labs.fr).

Each project deals with 1 automated module. For example the project Kolla aims
to deploy a fully functionnal Openstack solution.

## Project references

* [Infra_manager](https://gitlab.forge.orange-labs.fr/opnfv/infra_manager)
* [Kolla](https://gitlab.forge.orange-labs.fr/opnfv/kolla)
* [Rancher](https://gitlab.forge.orange-labs.fr/opnfv/rancher-k8s)
* [Kubespray](https://gitlab.com/Orange-OpenSource/lfn/infra/kubespray_automatic_installation)
* [OOM](https://gitlab.com/Orange-OpenSource/lfn/onap/onap_oom_automatic_installation/)
* [OS_infra_manager](https://gitlab.forge.orange-labs.fr/opnfv/os_infra_manager)
* [Functest](https://gitlab.forge.orange-labs.fr/opnfv/functest)
* [Xtesting_onap](https://gitlab.forge.orange-labs.fr/onap/xtesting-onap)

## Existing automated modules

### Infrastructure Installation

| Project | Target | Comments | Inputs |
|---------------|---------------|------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|
| Infra_manager | bare metal HW | Prepare the hardware, Configure a mgt IP reachable through SSH, Generate the POD inventory |  PDF & IDF files |
| Kolla | Openstack | | Inframanager |
| Rancher | Kubernetes | Historical kubernetes ONAP installer | Infra manager for installation on bare metal + Kolla artifacts for installation on VM |
| Kubespray | Kubernetes | ONAP installer we are using since Casablanca | Infra manager for installation on bare metal + Kolla artifacts for installation on VM |

PDF: Platform Description File

IDF: infrastructure Description File

DDF: Datacenter Description File

### Openstack resource creation

| Project | Target | Inputs |
|------------------|--------------------------------------------------|--------------------------------------|
| OS_infra_manager | Create ressources according to PDF and IDF files |  PDF file, IDF file, Kolla artifacts |

### ONAP Installation

| project | Target | Inputs |
|-----|-------------------------------------------------|-----------------------------------------------|
| OOM | Deploy ONAP based on the official OOM installer | PDF file, IDF file, DDF file, Kolla artifacts |

### Tests

| Project | target | Inputs |
|---------------|------------------------------------------------------------------------|---------------------------------------------|
| Functest | Test of the virtualization infrastructure (Openstack or k8s) | Kolla and/or k8s installer artifacts |
| Xtesting_ONAP | ONAP tests including k8s/helm, robot healthcheck, VNF tests | ONAP installation artifacts |

## Mix & match

You can chain the different modules. Of course it must be coherent, there is no
sense to test ONAP after an Openstack installation.
You need only 1 kubernetes installer, no need to chain the Rancher and Kubespray.

Some examples are described in the figure below:

![chained-ci-examples](img/chained-ci-examples.png)

It is possible to add additional module in chained-ci if needed, you can
contact the team.
